export default () => ({
  booksAll: [],
  currentBook: null,
  currentBookAvailableStores: [],
  currentBookHistory: null,
  currentBookActive: null,
  availableStores: null,
  loggedIn: false,
  currentUser: {
    id: -1,
    name: null,
    email: null
  }
})
