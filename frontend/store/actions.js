const backendUrl = 'http://localhost:8080/api/'

export default {
  login({ commit }, payload) {
    this.$axios.defaults.headers.common['Authorization'] = 'Basic ' + btoa(`${payload.uname}:${payload.pass}`);

    this.$axios.get(`${backendUrl}users/current-user`)
      .then(resp => {
        commit('updateCurrentUser', resp.data);
        commit('updateLoggedIn', true);
      })
      .catch(reason =>{
        console.warn(reason.message);
      })
  },
  logout({commit}) {
    this.$axios.defaults.headers.common['Authorization'] = null;
    commit('updateCurrentUser', {id: -1, name: null, email: null});
    commit('updateLoggedIn', false);
  },
  availableStores({ commit }) {
    this.$axios.get(`${backendUrl}stores`)
      .then(resp => {
        commit('updateAvailableStores', resp.data);
      });
  },
  booksAll({ commit }) {
    this.$axios.get(backendUrl + 'books')
      .then(resp => {
        commit('setBooksAll', resp.data)
      })
  },
  currentBook({ commit, dispatch }, payload) {
    commit('updateCurrentBook', null);
    commit('updateCurrentBookAvailableStores', []);
    this.$axios.get(`${backendUrl}books/${payload.isbn}`)
      .then(resp => {
        commit('updateCurrentBook', resp.data);
        if (payload.needDeepReload) {
          dispatch('currentBookAvailableStores', {e404handler: () => payload.e404handler()});
          dispatch('currentBookHistory', {e404handler: () => payload.e404handler()})
          dispatch('currentBookActive', {e404handler: () => payload.e404handler()})
        }
      })
    ;
  },
  currentBookAvailableStores({ commit, getters }, payload) {
    if (!getters.getCurrentBook) {
      console.error('There is no book in store!');
      return;
    }

    this.$axios.get(`${backendUrl}books/${getters.getCurrentBook.isbn}/available`)
      .then(resp => {
        commit('updateCurrentBookAvailableStores', resp.data);
      })
      .catch(reason => {
        if (reason.response.status === 404) {
          payload.e404handler()
        } else {
          console.warn(reason)
        }
      })
  },
  currentBookHistory({ commit, getters }, payload) {
    if (!getters.getCurrentBook) {
      console.error('There is no book in store!');
      return;
    }

    this.$axios.get(`${backendUrl}books/${getters.getCurrentBook.isbn}/history`)
      .then(resp => {
        commit('updateCurrentBookHistory', resp.data);
      })
      .catch(reason => {
        if (reason.response.status === 404) {
          payload.e404handler()
        } else {
          console.warn(reason)
        }
      })
  },
  currentBookActive({ commit, getters }, payload) {
    if (!getters.getCurrentBook) {
      console.error('There is no book in store!');
      return;
    }

    this.$axios.get(`${backendUrl}books/${getters.getCurrentBook.isbn}/active`)
      .then(resp => {
        commit('updateCurrentBookActive', resp.data);
      })
      .catch(reason => {
        if (reason.response.status === 404) {
          payload.e404handler()
        } else {
          console.warn(reason)
        }
      })
  },
  letsBook({}, payload) {
    this.$axios.post(`${backendUrl}items/${payload.id}`)
      .then(() => {
        payload.onSuccess()
      })
      .catch(reason => {
        payload.onFail()
        console.warn(reason)
      })
  },
  letsReturn({ getters }, payload) {
    this.$axios.post(`${backendUrl}books/return`, { isbn: getters.getCurrentBook.isbn, storeId: payload.storeId})
      .then(() => {
        payload.onSuccess()
      })
      .catch(reason => {
        payload.onFail()
        console.warn(reason)
      })
  }
}
