export default {
  setBooksAll(state, payload) {
    state.booksAll = payload
  },
  updateAvailableStores(state, payload) {
    state.availableStores = payload
  },
  updateCurrentBook(state, payload) {
    state.currentBook = payload
  },
  updateCurrentBookAvailableStores(state, payload) {
    state.currentBookAvailableStores = payload
  },
  updateCurrentBookHistory(state, payload) {
    state.currentBookHistory = payload
  },
  updateCurrentBookActive(state, payload) {
    state.currentBookActive = payload
  },
  updateCurrentUser(state, payload) {
    state.currentUser = payload
  },
  updateLoggedIn(state, payload) {
    state.loggedIn = payload
  }
}
