insert into profiles(id, name, email, password) values
(1, 'Mikhail Pashkov', 'workpashkovmikhail@gmail.com', '$2a$10$8ZLIZKUy4yp.yE4bDXk0PeBkJ1zBgpLNYqF8qWH6poHt2vXVTl/1m'),
(2, 'Nikita Bokhin', 'mrabudabu@yandex.ru', '$2a$10$8ZLIZKUy4yp.yE4bDXk0PeBkJ1zBgpLNYqF8qWH6poHt2vXVTl/1m'),
(3, 'Ivan Ivanov', 'ivanov@mail.ru', '$2a$10$8ZLIZKUy4yp.yE4bDXk0PeBkJ1zBgpLNYqF8qWH6poHt2vXVTl/1m'),
(4, 'Semen Semenov', 'semenov@yahoo.com', '$2a$10$8ZLIZKUy4yp.yE4bDXk0PeBkJ1zBgpLNYqF8qWH6poHt2vXVTl/1m'),
(5, 'Stepan Stepanov', 'stepanov@gmail.com', '$2a$10$8ZLIZKUy4yp.yE4bDXk0PeBkJ1zBgpLNYqF8qWH6poHt2vXVTl/1m');