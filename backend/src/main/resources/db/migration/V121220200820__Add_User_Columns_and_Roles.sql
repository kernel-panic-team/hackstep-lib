alter table profiles
    add
        email varchar(255);
alter table profiles
    add
        password varchar(255);

create table role (
    id bigserial primary key,
    name varchar(255)
);

insert into role(name) values ('ADMIN'), ('USER');

create table profiles_roles (
    profiles_id bigint references profiles(id),
    roles_id bigint references role(id)
)