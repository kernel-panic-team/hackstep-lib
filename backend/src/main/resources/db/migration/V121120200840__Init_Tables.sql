create table genre (
    id bigserial primary key not null,
    title varchar(255) unique not null,
    description text
);

create table book (
    isbn BIGINT primary key not null,
    title varchar(255) not null,
    year_published smallint,
    page_nums int,
    description text,
    image_url varchar(255),
    rating decimal(1) default(0)
);

create table book_genres (
    book_isbn BIGINT references book(isbn) not null,
    genre_id BIGINT references genre(id)
);

create table author (
    id serial primary key not null,
    name varchar(255) not null,
    description text
);

create table authors (
    isbn BIGINT references book(isbn) not null,
    author int references author(id) not null
);

create table item (
    id bigserial primary key not null,
    isbn BIGINT references book(isbn) not null
);

create table profiles (
    id bigserial primary key not null,
    name varchar(255) not null
);

create table store (
   id bigserial primary key not null,
   title varchar(255) not null,
   description text
);

create table active (
    id serial primary key not null,
    date_taken date not null,
    user_id int references profiles(id) not null,
    item_id int references item(id) not null
);

create table history (
    id bigserial primary key not null,
    date_taken date not null,
    date_returned date not null,
    store_id int references store(id),
    user_id int references profiles(id) not null,
    item_id int references item(id) not null
);

create table review (
    id bigserial primary key not null,
    isbn BIGINT references book(isbn) not null,
    entertainment_rate int,
    writing_style_rate int,
    practice_usage_rate int,
    review text
)


-- create type permission as enum ('BOOK_MANAGEMENT', 'USER_MANAGEMENT');
--
-- create table permissions (
--     user_id int references "user"(id),
--     permission_code permission
-- );

-- create table book_property (
--                                id serial primary key,
--                                description varchar(255)
-- );
--
-- create table book_properties (
--                                  isbn varchar(255) references book(isbn),
--                                  property_id int references book_property(id)
-- );

