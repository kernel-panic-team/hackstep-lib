package kernelpanic.hacksteplib.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class History {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate dateTaken;
    private LocalDate dateReturned;
    @Column(columnDefinition = "int4")
    private Long storeId;
    @Column(columnDefinition = "int4")
    private Long userId;
    @Column(columnDefinition = "int4")
    private Long itemId;

}
