package kernelpanic.hacksteplib.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Active {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate dateTaken;
    @Column(columnDefinition = "int4")
    private Long userId;
    @Column(columnDefinition = "int4")
    private Long itemId;


}
