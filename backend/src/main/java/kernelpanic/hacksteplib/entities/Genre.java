package kernelpanic.hacksteplib.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Data
public class Genre {

    @Id
    @Column
    private Long id;
    private String title;
    private String description;
    @ManyToMany(mappedBy = "genre")
    @JsonIgnore
    private List<Book> books;
}
