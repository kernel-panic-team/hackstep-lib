package kernelpanic.hacksteplib.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
public class Book {

    @Id
    private Long isbn;
    private String title;
    private short yearPublished;
    @ManyToMany
    @JoinTable(
        name = "book_genres",
        joinColumns = @JoinColumn(name = "book_isbn", columnDefinition = "int8"),
        inverseJoinColumns = @JoinColumn(name = "genre_id")
    )
    private List<Genre> genre;
    private int pageNums;
    private String imageUrl;
    private String description;
    @Column(columnDefinition = "numeric")
    private float rating;
    @ManyToMany
    @JoinTable(
            name = "authors",
            joinColumns = @JoinColumn(name = "isbn", columnDefinition = "int8"),
            inverseJoinColumns = @JoinColumn(name = "author")
    )
    private List<Author> authors;
}
