package kernelpanic.hacksteplib.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Store {

    @Id
    private Long id;
    private String title;
    private String description;

}
