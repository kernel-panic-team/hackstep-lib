package kernelpanic.hacksteplib.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
public class Item {

    @Id
    private Long id;
    @ManyToOne
    @JoinColumn(name = "isbn", insertable = false, updatable = false) // todo: there will be problems..
    private Book book;
    private Long isbn;
}
