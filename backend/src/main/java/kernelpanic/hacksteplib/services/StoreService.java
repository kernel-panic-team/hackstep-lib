package kernelpanic.hacksteplib.services;

import kernelpanic.hacksteplib.entities.Store;
import kernelpanic.hacksteplib.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class StoreService {

    @Autowired
    StoreRepository storeRepository;

    public List<Store> getAvailableStores() {
        return StreamSupport.stream(storeRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }
    
}
