package kernelpanic.hacksteplib.services;

import kernelpanic.hacksteplib.entities.Book;
import kernelpanic.hacksteplib.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public Iterable<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Optional<Book> getBookByIsbn(long isbn) {
        return bookRepository.findByIsbn(isbn);
    }
}
