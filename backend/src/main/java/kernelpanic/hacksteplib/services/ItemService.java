package kernelpanic.hacksteplib.services;

import kernelpanic.hacksteplib.entities.Active;
import kernelpanic.hacksteplib.entities.History;
import kernelpanic.hacksteplib.entities.Item;
import kernelpanic.hacksteplib.repositories.*;
import kernelpanic.hacksteplib.rmodels.RStoreByIsbn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ActiveRepository activeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HistoryRepository historyRepository;

    public Iterable<Item> getAllItems() {
        return itemRepository.findAll();
    }

    public Optional<Item> getItemById(long id) {
        return itemRepository.findById(id);
    }

    public Iterable<Item> getItemsByIsbn(long isbn) {
        return itemRepository.findByIsbn(isbn);
    }

    public int getTotalCount(long isbn) {
        return itemRepository.getTotalCount(isbn);
    }

    public int getTakenCount(long isbn) {
        return itemRepository.getTakenCount(isbn);
    }

    public List<RStoreByIsbn> getAvailableItemsAtStoreByIsbn(long isbn) {

        List<RStoreByIsbn> result = new ArrayList<>();

        // todo: rewrite...
        itemRepository.getAvailableItemsAtStoreByIsbn(isbn).forEach(it -> {
            Optional<RStoreByIsbn> storeByIsbn = result.stream().filter(r -> r.id.equals(it.getId())).findFirst();
            if (storeByIsbn.isPresent()) {
                storeByIsbn.get().items.add(it.getItemId());
            } else {
                RStoreByIsbn rStoreByIsbn = new RStoreByIsbn();
                rStoreByIsbn.id = it.getId();
                rStoreByIsbn.title = it.getTitle();
                rStoreByIsbn.items = new ArrayList<>(List.of(it.getItemId()));
                result.add(rStoreByIsbn);
            }
        });

        return result;
    }

    public void takeItem(long itemId, long userId) {
        Active record = new Active();
        record.setItemId(itemId);
        record.setUserId(userId);
        record.setDateTaken(LocalDate.now());

        activeRepository.save(record);
    }

    public void returnBook(long isbn, long storeId, long userId) {
        // todo: do this stuff by sql in transaction. Now it's not data safety
        historyRepository.saveAll(StreamSupport.stream(activeRepository.findAll().spliterator(), false)
                .filter(it -> it.getUserId() == userId && itemRepository.findById(it.getItemId()).orElseGet(Item::new).getIsbn() == isbn)
                .map(it -> {
                    History history = new History();
                    history.setDateTaken(it.getDateTaken());
                    history.setDateReturned(LocalDate.now());
                    history.setItemId(it.getItemId());
                    history.setUserId(it.getUserId());
                    history.setStoreId(storeId);

                    activeRepository.delete(it);

                    return history;
                })
                .collect(Collectors.toList())
        );


    }

    public List<HistoryByIsbnListModel> getHistoryByIsbn(long isbn) {
        return historyRepository.getHistoryByIsbn(isbn).stream().limit(10).collect(Collectors.toList());
    }

    public List<ActiveByIsbnListModel> getActiveByIsbn(long isbn) {
        return historyRepository.getActiveByIsbn(isbn);
    }
}
