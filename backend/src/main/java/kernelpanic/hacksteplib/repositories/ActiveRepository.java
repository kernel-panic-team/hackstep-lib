package kernelpanic.hacksteplib.repositories;

import kernelpanic.hacksteplib.entities.Active;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActiveRepository extends CrudRepository<Active, Integer> {
}
