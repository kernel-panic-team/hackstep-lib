package kernelpanic.hacksteplib.repositories;

import kernelpanic.hacksteplib.entities.History;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends CrudRepository<History, Integer> {
    @Query(value = "select new kernelpanic.hacksteplib.repositories.HistoryByIsbnListModel(" +
            "h.dateTaken, h.dateReturned, s.title, p.name, h.itemId) " +
            "from History h " +
            "         join Store s on s.id = h.storeId " +
            "         join Item i on i.id = h.itemId " +
            "         join profiles p on p.id = h.userId " +
            "         left join Active a on i.id = a.itemId " +
            "where i.isbn = :isbn " +
            "  and a.itemId is null " +
            "order by h.dateReturned ") // need set max results
    List<HistoryByIsbnListModel> getHistoryByIsbn(@Param("isbn") long isbn);

    @Query(value = "select new kernelpanic.hacksteplib.repositories.ActiveByIsbnListModel(" +
            "a.dateTaken, p.name, a.itemId) " +
            "from Active a " +
            "         join Item i on i.id = a.itemId " +
            "         join profiles p on p.id = a.userId " +
            "where i.isbn = :isbn " +
            "order by a.dateTaken ")
    List<ActiveByIsbnListModel> getActiveByIsbn(@Param("isbn") long isbn);
}
