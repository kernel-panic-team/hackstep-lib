package kernelpanic.hacksteplib.repositories;

import kernelpanic.hacksteplib.entities.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    Iterable<Item> findByIsbn(long isbn);

    Item getById(long id);

    @Query(value = "select count(*) from item i where i.isbn = :isbn", nativeQuery = true)
    Integer getTotalCount(@Param("isbn") Long isbn);

    /**
     * Need move this query to bookList
     *
     * @param isbn - book Isbn
     * @return count of available items by isbn
     */
    @Query(value = "select coalesce((" +
            "select count(*) " +
            "from active a " +
            "join item i on i.id = a.item_id " +
            "where i.isbn = :isbn " +
            "group by i.isbn), 0)", nativeQuery = true)
    Integer getTakenCount(@Param("isbn") Long isbn);

    @Query(value = "select new kernelpanic.hacksteplib.repositories.AvailableItemsAtStoreByIsbnListModel(h.storeId, s.title, i.id) " +
            "from History h " +
            "         join Store s on s.id = h.storeId " +
            "         join Item i on i.id = h.itemId " +
            "         left join Active a on i.id = a.itemId " +
            "where i.isbn = :isbn " +
            "  and a.itemId is null " +
            "  and h.id = (select max(hh.id) from History hh where hh.itemId = h.itemId group by hh.itemId)")
    List<AvailableItemsAtStoreByIsbnListModel> getAvailableItemsAtStoreByIsbn(@Param("isbn") Long isbn);

}
