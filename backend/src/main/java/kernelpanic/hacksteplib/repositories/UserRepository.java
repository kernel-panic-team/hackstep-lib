package kernelpanic.hacksteplib.repositories;

import kernelpanic.hacksteplib.entities.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<Profile, Long> {
    Profile findByEmail(String email);

    Profile getById(long id);
}
