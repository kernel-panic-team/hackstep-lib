package kernelpanic.hacksteplib.repositories;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class HistoryByIsbnListModel {
    private LocalDate dateTaken;
    private LocalDate dateReturned;
    private String storeTitle;
    private String userName;
    private long itemId;
}
