package kernelpanic.hacksteplib.repositories;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class ActiveByIsbnListModel {
    private LocalDate dateTaken;
    private String userName;
    private long itemId;
}
