package kernelpanic.hacksteplib.repositories;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AvailableItemsAtStoreByIsbnListModel {
    private Long id;
    private String title;
    private Long itemId;
}
