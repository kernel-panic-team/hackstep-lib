package kernelpanic.hacksteplib.repositories;

import kernelpanic.hacksteplib.entities.Store;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends CrudRepository<Store, Long> {

}
