package kernelpanic.hacksteplib.rmodels;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Data
public class RStoreItem {
    @Id
    private Integer storeId;
    private Integer itemId;
    private String title;
//    private List<Long>

}

