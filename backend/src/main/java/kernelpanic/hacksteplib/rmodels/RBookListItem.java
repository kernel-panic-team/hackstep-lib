package kernelpanic.hacksteplib.rmodels;

import lombok.Data;

import java.util.List;

@Data
public class RBookListItem {
    public Long isbn;
    public String title;
    public String imageUrl;
    public String description;
    public List<RBookAuthor> authors;

    public int pageNums;
    public short yearPublished;
    public List<RBookGenre> genre;

    public int total;
    public int available;

    public float rating;
}
