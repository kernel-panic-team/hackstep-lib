package kernelpanic.hacksteplib.rmodels;

import lombok.Data;

import java.util.List;

@Data
public class RStoreByIsbn {
    public Long id;
    public String title;
    public List<Long> items;
}
