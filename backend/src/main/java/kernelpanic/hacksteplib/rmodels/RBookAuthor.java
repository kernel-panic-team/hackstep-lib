package kernelpanic.hacksteplib.rmodels;

import lombok.Data;

@Data
public class RBookAuthor {
    public int id;
    public String name;
}
