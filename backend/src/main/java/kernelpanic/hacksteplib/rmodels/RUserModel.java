package kernelpanic.hacksteplib.rmodels;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class RUserModel {

    @NotNull
    private String name;

    @NotNull
    private String email;
    @NotNull
    private String password;
}
