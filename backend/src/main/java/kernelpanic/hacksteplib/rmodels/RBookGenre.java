package kernelpanic.hacksteplib.rmodels;

import lombok.Data;

@Data
public class RBookGenre {
    public long id;
    public String title;
}
