package kernelpanic.hacksteplib.controllers;

import kernelpanic.hacksteplib.entities.Author;
import kernelpanic.hacksteplib.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/authors")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping
    public Iterable<Author> getAllAuthors() {
        return authorService.getAllAuthors();
    }

    @GetMapping("{authorId}")
    public Optional<Author> getAuthorById(@PathVariable int authorId) {
        return authorService.getAuthorById(authorId);
    }
}
