package kernelpanic.hacksteplib.controllers;

import com.sun.istack.NotNull;
import kernelpanic.hacksteplib.entities.Book;
import kernelpanic.hacksteplib.entities.History;
import kernelpanic.hacksteplib.repositories.ActiveByIsbnListModel;
import kernelpanic.hacksteplib.repositories.HistoryByIsbnListModel;
import kernelpanic.hacksteplib.rmodels.RBookAuthor;
import kernelpanic.hacksteplib.rmodels.RBookGenre;
import kernelpanic.hacksteplib.rmodels.RBookListItem;
import kernelpanic.hacksteplib.rmodels.RStoreByIsbn;
import kernelpanic.hacksteplib.services.BookService;
import kernelpanic.hacksteplib.services.ItemService;
import kernelpanic.hacksteplib.services.StoreService;
import kernelpanic.hacksteplib.services.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private UserService userService;

    @GetMapping
    public List<RBookListItem> getAllBooks() {
        return toRBookListItem(bookService.getAllBooks());
    }

    @GetMapping("{bookIsbn}")
    public Optional<Book> getBookByIsbn(@PathVariable long bookIsbn) {
        return bookService.getBookByIsbn(bookIsbn);
    }

    @GetMapping("{bookIsbn}/available")
    public List<RStoreByIsbn> getAvailableItemsAtStoreByIsbn(@PathVariable long bookIsbn) {
        return itemService.getAvailableItemsAtStoreByIsbn(bookIsbn);
    }

    @GetMapping("{bookIsbn}/history")
    public List<HistoryByIsbnListModel> getHistoryByIsbn(@PathVariable long bookIsbn) {
        return itemService.getHistoryByIsbn(bookIsbn);
    }

    @GetMapping("{bookIsbn}/active")
    public List<ActiveByIsbnListModel> getActiveByIsbn(@PathVariable long bookIsbn) {
        return itemService.getActiveByIsbn(bookIsbn);
    }

    @PostMapping("return")
    public void returnBook(@RequestBody ReturnBookRequestBody body) {
        Long userId = (Long) userService.getCurrentUser().get("id");
        // todo: maybe it'll be good to check that user can return something here
        itemService.returnBook(body.isbn, body.storeId, userId);
    }

    @Data
    private static class ReturnBookRequestBody {
        @NotNull
        private long isbn;
        @NotNull
        private long storeId;
    }

    private List<RBookListItem> toRBookListItem(Iterable<Book> books) {
        return StreamSupport.stream(books.spliterator(), false)
                .map(this::toRBookListItem)
                .collect(Collectors.toList());
    }

    private RBookListItem toRBookListItem(Book it) {
        RBookListItem rbl = new RBookListItem();
        rbl.isbn = it.getIsbn();
        rbl.title = it.getTitle();
        rbl.imageUrl = it.getImageUrl();
        rbl.description = it.getDescription();
        rbl.authors = it.getAuthors().stream().map(this::toRBookAuthor).collect(Collectors.toList());
        rbl.pageNums = it.getPageNums();
        rbl.yearPublished = it.getYearPublished();
        rbl.genre = it.getGenre().stream().map(this::toRBookGenre).collect(Collectors.toList());
        rbl.total = itemService.getTotalCount(it.getIsbn());
        rbl.available = rbl.total - itemService.getTakenCount(it.getIsbn());
        rbl.rating = it.getRating();
        return rbl;
    }

    private RBookGenre toRBookGenre(kernelpanic.hacksteplib.entities.Genre it) {
        RBookGenre rbg = new RBookGenre();
        rbg.id = it.getId();
        rbg.title = it.getTitle();
        return rbg;
    }

    private RBookAuthor toRBookAuthor(kernelpanic.hacksteplib.entities.Author it) {
        RBookAuthor rba = new RBookAuthor();
        rba.id = it.getId();
        rba.name = it.getName();
        return rba;
    }
}
