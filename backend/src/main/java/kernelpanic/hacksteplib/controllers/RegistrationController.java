package kernelpanic.hacksteplib.controllers;

import kernelpanic.hacksteplib.entities.Profile;
import kernelpanic.hacksteplib.rmodels.RUserModel;
import kernelpanic.hacksteplib.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/register")
public class RegistrationController {
    @Autowired
    private UserService userService;

    @PostMapping
    public String registerUser(@RequestBody RUserModel rUserModel) {
        Profile profile = new Profile();
        profile.setEmail(rUserModel.getEmail());
        profile.setPassword(rUserModel.getPassword());
        profile.setName(rUserModel.getName());
        if (userService.saveUser(profile)) {
            return "registered";
        }
        return "error";
    }
}
