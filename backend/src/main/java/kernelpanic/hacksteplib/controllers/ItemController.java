package kernelpanic.hacksteplib.controllers;

import kernelpanic.hacksteplib.entities.Item;
import kernelpanic.hacksteplib.services.ItemService;
import kernelpanic.hacksteplib.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/items")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private UserService userService;

    @GetMapping
    public Iterable<Item> getAllItems() {
        return itemService.getAllItems();
    }

    @GetMapping("{itemId}")
    public Optional<Item> getItemById(@PathVariable long itemId) {
        return itemService.getItemById(itemId);
    }

    @PostMapping("{itemId}")
    public void bookTheItemById(@PathVariable long itemId) {
        Long userId = (Long) userService.getCurrentUser().get("id");
        // todo: check if user already has this book
        itemService.takeItem(itemId, userId);
    }
}
