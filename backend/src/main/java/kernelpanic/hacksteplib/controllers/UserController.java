package kernelpanic.hacksteplib.controllers;

import kernelpanic.hacksteplib.entities.Profile;
import kernelpanic.hacksteplib.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Iterable<Profile> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("{userId}")
    public Optional<Profile> getUserById(@PathVariable long userId) {
        return userService.getUserById(userId);
    }

    @GetMapping("current-user")
    public Map<String, Object> getCurrentUser() {
        return userService.getCurrentUser();
    }
}
