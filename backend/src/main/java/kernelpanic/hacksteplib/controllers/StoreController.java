package kernelpanic.hacksteplib.controllers;

import kernelpanic.hacksteplib.entities.Store;
import kernelpanic.hacksteplib.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stores")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @GetMapping
    public List<Store> getAvailableStores() {
        return storeService.getAvailableStores();
    }

}
