# hackstep lib

## backend

Backend на `Spring Framework`, используется `Maven` для управления зависимостями

backend запускается на `8080` порту

### ORM

Используем Hibernate с Postgresql

### Authentication & Authorization

Реализовано на basic auth

## frontend

Используется `Nuxt.js` фреймворк и `Buefy` в качестве UI фреймворка

Взаимодействие с backend'ом через action'ы vuex'а

frontend запускается на `8080` порту (`npm start / npm run dev`)
