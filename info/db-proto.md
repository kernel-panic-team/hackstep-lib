book
- isbn
- title
- genre fk
- page_nums
- description
- image-url
- rating (1-10 e.g 5.2 )

// нужно добавить систему отзывов на книжки на сколько полезна для применения в работе
review
- isbn
(придумать к рейтингу еще 3-4 критерия. т.е. будет общая оценка и 3-4 другие. Например: Легкость чтения, кол-во воды, Субъективная полезность, практическая применимость)
можно это всё во многие-ко-многим положить

authors
- isbn
- author

author
- id
- fio
- desc

genre
- id
- title
- desc
 
item
- id
- isbn fk
- // where is this book located? think about it

store (место где может хранится книга. Шкаф в коридоре, полка над столом и т.п)
- id
- title
- desc

active - то, что сейчас на руках
- id
- date_taken
- user_id
- item_id
 
history - информация о прошлых выдачах
- id
- date_taken
- date_returned
- store_id
- user_id
- item_id

user
- id
- fio
- department fk
 
 
// optional
 
book_properties (?)
- isbn
- property_id
- property_value
 
book_property (?)
- id
- desc 

permissions
- user_id
- permission_code (BOOK_MANAGEMENT, USER_MANAGEMENT)
